package com.selenium.automation.util.verify;

import org.testng.Assert;

public class Verify {

    public static void verifyEquals(String actual, String expected, String message) {
        Assert.assertEquals(actual, expected, message);
    }

    public static void verifyFalse(boolean condition, String message) {
        Assert.assertFalse(condition, message);
    }

    public static void verifyTrue(boolean codition, String message) {
        Assert.assertTrue(codition, message);
    }

}
