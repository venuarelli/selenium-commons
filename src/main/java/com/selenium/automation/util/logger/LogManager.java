package com.selenium.automation.util.logger;

import org.apache.log4j.Logger;
import org.testng.Reporter;

public class LogManager {

    /**
     * factory.
     * @param clazz class.
     * @return {@link BaseLogger}
     */
    public static LogManager getLogger(final Class<?> clazz) {
        return new LogManager(clazz);
    }

    /**
     * {@link Logger}
     */
    private final Logger logger;

    /**
     * Constructor.
     * @param clazz Class
     */
    LogManager(final Class<?> clazz) {
        this.logger = Logger.getLogger(clazz);
    }

    /**
     * Log a message object with the DEBUG level.
     * @param message message to log.
     */
    public void logDebug(final String message) {
        logger.debug(message);
        Reporter.log(message);
    }

    /**
     * Log a message object with the ERROR level.
     * @param message message to log
     */
    public void logError(final String message) {
        logger.error(message);
        Reporter.log(message);
    }

    /**
     * Log a message object with FATAL level.
     * @param message
     */
    public void logFatal(final String message) {
        logger.fatal(message);
        Reporter.log(message);
    }

    /**
     * Log a message object with the INFO level.
     * @param message message to log.
     */
    public void logInfo(final String message) {
        logger.info(message);
        Reporter.log(message);
    }

    /**
     * Logger for setup methods.
     * @param message message to log.
     */
    public void logSetupStep(final String message) {
        logInfo("SetUp: " + message);
    }

    /**
     * Logger for tear down methods.
     * @param message message to log.
     */
    public void logTearDownStep(final String message) {
        logInfo("TearDown: " + message);
    }

    /**
     * Logger for test step.
     * @param message message to log.
     */
    public void logTestStep(final String message) {
        logInfo(message);
    }

    /**
     * Logger for tear down methods.
     * @param message message to log.
     */
    public void logTestVerificationStep(final String message) {
        logInfo("Verification: " + message);
    }

    /**
     * Log a message object with the WARN level.
     * @param message message to log.
     */
    public void logWarn(final String message) {
        logger.warn(message);
        Reporter.log(message);
    }

}
