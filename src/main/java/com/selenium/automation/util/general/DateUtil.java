package com.selenium.automation.util.general;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public String convertDateObjectToString(Date date, String format) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);

    }

    public Date dateStringToObject(String dateString, String format) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return date;

    }

    public String getTodayDateAsString() {
        return getTodayDateAsString("yyyyMMdd");
    }

    public String getTodayDateAsString(String format) {
        final Date date = new Date();
        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
}
