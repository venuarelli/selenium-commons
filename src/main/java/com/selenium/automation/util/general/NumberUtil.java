package com.selenium.automation.util.general;

import org.apache.commons.lang3.RandomStringUtils;

public class NumberUtil {

    public String getAlphabetic(int count) {
        return RandomStringUtils.randomAlphabetic(count);
    }

    public String getRandomAlphanumeric(int count) {
        return RandomStringUtils.randomAlphanumeric(count);
    }

    public String getRandomNumbers(int count) {
        return RandomStringUtils.randomNumeric(count);
    }
}
