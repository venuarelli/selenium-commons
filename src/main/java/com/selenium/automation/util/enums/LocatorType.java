package com.selenium.automation.util.enums;

public enum LocatorType {
    ID, NAME, CLASSNAME, TAGNAME, CSSSELECTOR, XPATH, LINKTEXT, PARTIALLINKTEXT;
}
