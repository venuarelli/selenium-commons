package com.selenium.automation.util.enums;

public enum BrowserType {

    CHROME, FIREFOX, IE, HEADLESS;

}
