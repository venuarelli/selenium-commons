package com.selenium.automation.util.reader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

public class PropertyReader {
    public static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    public static String getProperty(String key) {
        String value = System.getProperty(key);
        if (value == null || value.trim().length() == 0) {
            value = resourceBundle.getString(key);
        }
        return value;
    }

    public static Properties loadProperty(String filePath) {
        Properties properties = null;
        try {
            properties = new Properties();
            final FileInputStream fis = new FileInputStream(filePath);
            properties.load(fis);
        } catch (final IOException ie) {
            ie.printStackTrace();
        }
        return properties;
    }

}
